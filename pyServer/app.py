import os
import glob
import RPi.GPIO as GPIO
from bluetooth import *
import json
from pymongo import MongoClient
import datetime
import time
from bson import json_util

def isValidUser(db, uid):
	print "[INFO]: Authenticating.." 
	query = {}
	query["UserId"] = uid
	users = db.User.find(query)

	if (users.count() >= 1):
    		studentdata = users[0]
		return studentdata;
	else:
		return None;


def markAttendance(db, incuid, incimei, inccourseid, incsem, incyear):
	status = str("Unknown error!")

	dt = datetime.datetime.now()
        dateEntry = dt.date().strftime('%Y-%m-%d')
        timeEntry = time.mktime(dt.timetuple())

        #Mark the attendance
        query = {}
        query["CourseId"] = inccourseid
        query["Semester"] = incsem
        query["Year"]     = incyear
        course = db.Attendances.find(query)

        studentList = []
        studentList.append({"UserId":incuid, "EntryTime":timeEntry})

	if(course.count() == 0):
                #CASE 1: No attendance record for this course

                record = {}
                record["CourseId"] = inccourseid
                record["Semester"] = incsem
                record["Year"] = incyear
                record["Attendances"] = []
                record["Attendances"].append({"Date":dateEntry, "StudentData":studentList})
                db.Attendances.insert_one(record)
                print "[ATTENDANCE MARKED] New Record created for the course"
		status = str("SUCCESS!")
		return status;
	else:
                jsonstr = json_util.dumps(course[0])
                rjson   = json.loads(jsonstr)

                date_present = 0
                counter = -1
                for obj in rjson["Attendances"]:
                        counter += 1
                        if(obj["Date"]==str(dateEntry)):
                                date_present = 1
                                break

                if(date_present == 0):
                        #CASE 2: Attendance record is not present for the given date

                        rjson["Attendances"].append({"Date":dateEntry, "StudentData":studentList})
                        db.Attendances.find_one_and_update(query, {'$set': {"Attendances" : rjson["Attendances"]}})
                        print "[ATTENDANCE MARKED]: Record updated for the given date"
			status = str("SUCCESS!")
                else:
                        user_present = 0
                        for obj in rjson["Attendances"][counter]["StudentData"]:
                                if(obj["UserId"]==str(incuid)):
                                        user_present = 1
                                        break


                        if(user_present==0):
                                #CASE 3: No attendance record in the studentlist

                                rjson["Attendances"][counter]["StudentData"].append({"UserId":incuid,"EntryTime":timeEntry})
                                db.Attendances.find_one_and_update(query, {'$set': {"Attendances" : rjson["Attendances"]}})
                                print "[ATTENDANCE MARKED]: marked in the existing record"
				status = str("SUCCESS!")

                        else:
                                #CASE 4: Record already present
                                print "[ERROR]: Attendance already marked"
				status = str("Attendance already marked!")

		return status;



def runServer():
	#client = MongoClient()
	client = MongoClient('mongodb://maverick1234:1234@ds161225.mlab.com:61225/cmpe273-db')
	db = client['cmpe273-db']
	if(db is None):
		print "[ERROR]: Created a mongo client"
	else:
		print "[INFO]: Created a mongo client"


	print "[INFO]: Started PyBlueServer Event Loop"
	while True:    
		print " "   
		print "[INFO]: ----------------Waiting for Incoming connections--------------"

		clientSocket, client_info = serverSocket.accept()
		print "[INFO] Connection Accepted for client ", client_info

		try:
	        	incdata = clientSocket.recv(1024)
        		if(len(incdata) == 0): 
				break
	        	print "[INFO] Incoming Data: %s" % incdata

			incjson     = json.loads(incdata)
			print incjson
        		incuid      = json.dumps(incjson["UserId"]).strip('"')
        		incimei     = json.dumps(incjson["IMEI"]).strip('"')
        		inccourseid = json.dumps(incjson["CourseId"]).strip('"')
        		incsem      = json.dumps(incjson["Semester"]).strip('"')
        		incyear     = json.dumps(incjson["Year"]).strip('"')

        		print "[INFO]: Incoming JSON:"
        		print "======================"
        		print "[INFO]: incuid      = %s" % incuid
        		print "[INFO]: incimei     = %s" % incimei
        		print "[INFO]: inccourseid = %s" % inccourseid
        		print "[INFO]: incsem      = %s" % incsem
        		print "[INFO]: incyear     = %s" % incyear

        		studentdata = isValidUser(db, incuid)
        		if(studentdata == None):
                		print "[ERROR]: Invalid userid.. Try again"
	        		clientSocket.send("Invalid userid.. Try again")
				clientSocket.close()
                		continue
        		else:
                		print "[INFO]: valid uid"
                		imei = json.dumps(studentdata["IMEI"]).strip('"').strip('[]').strip('"')
                		if(incimei != imei):
                        		print "[ERROR]: Unauthorized Phone.. register this phone first"
					clientSocket.send(str("Unauthorized Phone.. register this phone first"))
					clientSocket.close()
                        		continue

			rc = markAttendance(db, incuid, incimei, inccourseid, incsem, incyear)
	        	clientSocket.send(str(rc))
			clientSocket.close()

		except IOError:
			pass

		except KeyboardInterrupt:
			print "[INFO] Connection closed"
			clientSocket.close()
			serverSocket.close()
			print "[INFO]: PyBlueServer Closed"

			break


if __name__ == "__main__":

	serverSocket=BluetoothSocket( RFCOMM )
	serverSocket.bind(("",PORT_ANY))
	serverSocket.listen(1)

	port = serverSocket.getsockname()[1]

	uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

	advertise_service( serverSocket, "AquaPiServer",
                   	   service_id = uuid,
                   	   service_classes = [ uuid, SERIAL_PORT_CLASS ],
                    	   profiles = [ SERIAL_PORT_PROFILE ],
                         )

	print "[INFO]: Starting PyBlueServer"
	runServer()
