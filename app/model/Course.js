"use strict";
var Course = (function () {
    function Course(id, course_num, section, prof, semester) {
        this.id = id;
        this.course_num = course_num;
        this.section = section;
        this.prof = prof;
        this.semester = semester;
    }
    return Course;
}());
exports.Course = Course;
//# sourceMappingURL=Course.js.map