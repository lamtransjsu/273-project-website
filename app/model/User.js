"use strict";
var User = (function () {
    function User(id, type, userName, firstName, lastName) {
        this.id = id;
        this.type = type;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map