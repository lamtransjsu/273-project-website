"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var Course_1 = require("../model/Course");
var TimeSlot_1 = require("../model/TimeSlot");
var COURSES_LIST = [
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016'),
    new Course_1.Course('1', 'CMPE 273', '01', 'C2', 'Spring 2016')
];
var TIME_SLOT_LIST = [new TimeSlot_1.TimeSlot('1', 'Sun Nov 20 2016 23:46:53 GMT-0800 (Pacific Standard Time)')];
var MainService = (function () {
    function MainService(http, router) {
        this.http = http;
        this.router = router;
    }
    MainService.prototype.getCourseList = function (id) {
        return COURSES_LIST;
    };
    MainService.prototype.getTimeSlot = function (id) {
        return TIME_SLOT_LIST;
    };
    MainService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, router_1.Router])
    ], MainService);
    return MainService;
}());
exports.MainService = MainService;
//# sourceMappingURL=main.service.js.map