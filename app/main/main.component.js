"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Helper_1 = require("../Helper");
var router_1 = require("@angular/router");
var MainComponent = (function () {
    function MainComponent(route) {
        this.route = route;
    }
    MainComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = params['id'];
            _this.type = localStorage.getItem(Helper_1.Helper.USER_TYPE_KEY);
            _this.title = _this.type == '0' ? 'STUDENT VIEW' : 'PROFESSOR VIEW';
        });
    };
    MainComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'main',
            templateUrl: 'main.component.html',
            styleUrls: ['main.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute])
    ], MainComponent);
    return MainComponent;
}());
exports.MainComponent = MainComponent;
//# sourceMappingURL=main.component.js.map