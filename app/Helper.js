"use strict";
var BASE_URL = 'https://project-9827f.firebaseio.com/';
var Helper = (function () {
    function Helper() {
    }
    Helper.buildUrl = function (url) {
        if (url === void 0) { url = ''; }
        return BASE_URL + url + '.json';
    };
    ;
    Helper.USER_NAME_KEY = 'currentUser_type';
    Helper.USER_TYPE_KEY = 'currentUser_username';
    return Helper;
}());
exports.Helper = Helper;
//# sourceMappingURL=Helper.js.map