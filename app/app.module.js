"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
var http_1 = require("@angular/http");
var course_list_row_1 = require("./course_picker/course_list_row/course_list_row");
var forms_1 = require("@angular/forms");
var login_component_1 = require("./login/login.component");
var login_service_1 = require("./login/login.service");
var time_slot_row_component_1 = require("./main/time-slot-list/time_slot_row/time-slot-row.component");
var course_detail_component_1 = require("./course_detail/course-detail.component");
var course_detail_service_1 = require("./course_detail/course-detail.service");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var course_picker_component_1 = require("./course_picker/course-picker.component");
var main_component_1 = require("./main/main.component");
var main_service_1 = require("./main/main.service");
var time_slot_list_component_1 = require("./main/time-slot-list/time-slot-list.component");
var main_routing_module_1 = require("./main/main-routing.module");
var dashboard_component_1 = require("./main/dashboard/dashboard.component");
var ng2_charts_1 = require("ng2-charts");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                ng2_charts_1.ChartsModule,
                app_routing_module_1.AppRoutingModule,
                main_routing_module_1.MainRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent,
                course_list_row_1.CourseListRowComponent,
                login_component_1.LoginComponent,
                time_slot_row_component_1.TimeSlotRowComponent,
                course_detail_component_1.CourseDetailComponent,
                course_picker_component_1.CoursePickerComponent,
                main_component_1.MainComponent,
                time_slot_list_component_1.TimeSlotListComponent,
                dashboard_component_1.DashboardComponent
            ],
            providers: [
                login_service_1.LoginService,
                course_detail_service_1.CourseDetailService,
                main_service_1.MainService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map