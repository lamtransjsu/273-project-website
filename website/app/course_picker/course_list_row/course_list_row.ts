import {Component, Input, OnInit} from "@angular/core";
import {Course} from "../../model/Course";
import {COURSE_YEAR_SELECT_KEY, COURSE_SEM_SELECT_KEY, COURSE_SELECT_KEY} from "../../Helper";
@Component({
    moduleId: module.id,
    selector: 'courses-list-row',
    templateUrl: 'course_list_row.component.html'
})

export class CourseListRowComponent implements OnInit {
    @Input() course: Course;

    ngOnInit(): void {
    }

    onRowClick(): void {
        localStorage.setItem(COURSE_YEAR_SELECT_KEY, this.course.year);
        localStorage.setItem(COURSE_SEM_SELECT_KEY, this.course.semester);
        localStorage.setItem(COURSE_SELECT_KEY, this.course.id);
    }
}
