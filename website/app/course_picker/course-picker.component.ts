import {Component, OnInit} from "@angular/core";
import {USER_TYPE_KEY, COURSE_SELECT_KEY, USER_ID_KEY} from "../Helper";
import {Course} from "../model/Course";
import {MainService} from "../main/main.service";
@Component({
    moduleId: module.id,
    selector: 'main',
    templateUrl: 'course-picker.component.html',
    styleUrls: ['course-picker.component.css']
})

export class CoursePickerComponent implements OnInit {
    private type: string;
    private title: string;
    private courseList: Course[];

    constructor(private mainService: MainService) {
    }

    ngOnInit(): void {
        this.type = localStorage.getItem(USER_TYPE_KEY);
        this.title = this.type == '0' ? 'STUDENT VIEW' : 'PROFESSOR VIEW';

        localStorage.removeItem(COURSE_SELECT_KEY);
        let userId = localStorage.getItem(USER_ID_KEY);
        this.mainService.getCourseList(userId).then((response) => {
            this.courseList = response;
        });
    }

}
