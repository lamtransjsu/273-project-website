/**
 * Created by lam on 11/22/2016.
 */
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {CoursePickerComponent} from "./course_picker/course-picker.component";
import {CourseRegistrationComponent} from "./course_registration/course_registration.component";
const appRoutes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'course-picker', component: CoursePickerComponent},
    {path: 'course-registration', component: CourseRegistrationComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}
