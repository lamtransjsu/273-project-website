import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Router} from "@angular/router";
import {Helper, USER_ID_KEY, USER_FIRST_NAME_KEY, USER_LAST_NAME_KEY} from "../Helper";
import "rxjs/add/operator/toPromise";


@Injectable()
export class LoginService {

    constructor(private http: Http, private router: Router) {
    }

    validateLogin(username: string, password: string): Promise<Boolean> {
        let url = Helper.buildUrl('login/');
        let postPayload = JSON.stringify({UserId: username, Password: password});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let res = response.json();
                    localStorage.setItem(USER_ID_KEY, res['UserId']);
                    console.log(res['UserId']);
                    localStorage.setItem(USER_FIRST_NAME_KEY, res['FirstName']);
                    localStorage.setItem(USER_LAST_NAME_KEY, res['LastName']);

                    return true;
                }
                return false;
            });
    }
}