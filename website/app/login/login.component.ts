import {Component, OnInit} from "@angular/core";
import {LoginService} from "./login.service";
import {Router} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit {
    private username;
    private password;
    private loginFail: boolean = false;

    constructor(private router: Router, private loginService: LoginService) {
    }

    onSubmit(): void {
        this.loginService.validateLogin(this.username, this.password).then((response) => {
            if (response) {
                this.router.navigate(['/course-picker']);
            }

            this.loginFail = true;
        });

    }

    ngOnInit(): void {
    }
}
