// const BASE_URL = 'http://localhost:3000/web/';
const BASE_URL = 'https://appserver-raspberry.herokuapp.com/web/';
export const USER_ID_KEY = 'currentUser_id';
export const USER_FIRST_NAME_KEY = 'currentUser_firstName';
export const USER_LAST_NAME_KEY = 'currentUser_lastName';
export const USER_TYPE_KEY = 'currentUser_username';

export const COURSE_SELECT_KEY = 'current_course_id';
export const COURSE_YEAR_SELECT_KEY = 'current_course_year';
export const COURSE_SEM_SELECT_KEY = 'current_course_semester';

export class Helper {

    static buildUrl(url: String = '') {
        return BASE_URL + url;
    };
}