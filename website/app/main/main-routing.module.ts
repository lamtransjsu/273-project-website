/**
 * Created by lam on 11/22/2016.
 */
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {MainComponent} from "./main.component";
import {TimeSlotListComponent} from "./time-slot-list/time-slot-list.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {StudentListEditComponent} from "./student-list-edit/student-list-edit.component";
import {StatByDateComponent} from "./time-slot-list/stat-by-date/stat-by-date.component";
const appRoutes = [
    {
        path: 'main/:id',
        component: MainComponent,
        children: [
            {path: '', redirectTo: 'dashboard'},
            {
                path: 'time-slots', children: [
                {path: '', component: TimeSlotListComponent},
                {path: 'stat-by-date', component: StatByDateComponent},
            ]
            },
            {path: 'edit-student-list', component: StudentListEditComponent},
            {path: 'dashboard', component: DashboardComponent}
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule {
}
