import {Component, OnInit} from "@angular/core";
import {MainService} from "../main.service";
import {COURSE_SELECT_KEY} from "../../Helper";
@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styles: ['.chart {display: block; width: 100%;}']
})

export class DashboardComponent implements OnInit {

    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public lineChartType: string = 'line';

    constructor(private mainService: MainService) {
    }

    ngOnInit(): void {
        let courseId = localStorage.getItem(COURSE_SELECT_KEY);
        this.mainService.getDashboardInfo(courseId).then((response) => {
            this.lineChartLabels = response[0];
            this.lineChartData = response[1];
        });
    }

    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }
}
