import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'main',
    templateUrl: 'main.component.html',
    styleUrls: ['main.component.css']
})

export class MainComponent implements OnInit {
    constructor(private route: ActivatedRoute) {
    }

    ngOnInit(): void {
    }

}
