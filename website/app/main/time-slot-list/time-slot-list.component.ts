import {Component, OnInit, Input} from "@angular/core";
import {TimeSlot} from "../../model/TimeSlot";
import {MainService} from "../main.service";
import {COURSE_SELECT_KEY} from "../../Helper";
@Component({
    moduleId: module.id,
    selector: 'time-slot-list',
    templateUrl: 'time-slot-list.component.html',
})

export class TimeSlotListComponent implements OnInit {
    private timeSlotList: TimeSlot[];
    @Input() courseId: string;

    constructor(private mainService: MainService) {
    }

    ngOnInit(): void {
        this.courseId = localStorage.getItem(COURSE_SELECT_KEY);
        this.mainService.getTimeSlot(this.courseId).then((response) => {
            this.timeSlotList = response;
        });
    }
}
