import {Component, OnInit} from "@angular/core";
import {MainService} from "../../main.service";
import {ActivatedRoute} from "@angular/router";
import "rxjs/add/operator/map";

@Component({
    moduleId: module.id,
    selector: 'stat-by-date',
    templateUrl: 'stat-by-date.component.html'
})

export class StatByDateComponent implements OnInit {
    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public dataInTimeFormat: Array<any>;
    public lineChartType: string = 'bar';

    // options = {
    //     scales: {
    //         yAxes: [
    //             {
    //                 ticks: {
    //                     callback: (v) => this.formatSecsAsMins(v),
    //                     stepSize: 300, //add a tick every 5 minutes
    //                 }
    //             }
    //         ]
    //     },
    // };

    private date: string;

    constructor(private mainService: MainService, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.route
            .queryParams
            .map(params => params['time'] || 'None')
            .subscribe(result => this.date = result);

        this.mainService.getStudentListByDate(this.date).then((response) => {
            this.lineChartLabels = response[0];
            this.lineChartData = response[1];
            this.dataInTimeFormat = response[2];
        });
    }
}
