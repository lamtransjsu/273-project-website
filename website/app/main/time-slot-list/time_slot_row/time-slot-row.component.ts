import {Component, Input, OnInit} from "@angular/core";
import {TimeSlot} from "../../../model/TimeSlot";
@Component({
    moduleId: module.id,
    selector: 'time-slot-row',
    templateUrl: 'time-slot-row.component.html',
})

export class TimeSlotRowComponent implements OnInit {
    @Input() timeSlot: TimeSlot;


    ngOnInit(): void {
    }
}
