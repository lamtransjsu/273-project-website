import {Component, OnInit} from "@angular/core";
import {MainService} from "../main.service";
import {Student} from "../../model/Student";
import {ActivatedRoute} from "@angular/router";
@Component({
    moduleId: module.id,
    selector: 'student-list-edit',
    templateUrl: 'student-list-edit.component.html',
    styles: ['.addStudent {margin-top: 20px}  textarea {width:60%; resize: none; margin-top: 5px}']
})

export class StudentListEditComponent implements OnInit {
    private students: Student[];
    private studentType: string = '1';

    protected student: Object = {};

    constructor(private mainService: MainService, private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            console.log(params);
        });
    }

    ngOnInit(): void {
        this.getStudents();
    }

    getStudents(): void {
        this.mainService.getStudentList().then((response) => {
            this.students = response;
        });
    }

    onNewStudentAdded(): void {
        this.mainService.addStudentToCourse(this.student['id']).then((response) => {
            if(response) {
                this.getStudents();
            }
        });
    }

    removeRow(studentId): void {
        this.mainService.removeStudent(studentId).then(response => {
            this.getStudents();
            // delete(this.student.id);
        });
    }
}
