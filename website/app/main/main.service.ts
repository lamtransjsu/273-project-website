import {Injectable} from "@angular/core";
import {Http, Headers, RequestOptions} from "@angular/http";
import {Router} from "@angular/router";
import {Course} from "../model/Course";
import {TimeSlot} from "../model/TimeSlot";
import {Student} from "../model/Student";
import {Helper, COURSE_SELECT_KEY, COURSE_YEAR_SELECT_KEY, COURSE_SEM_SELECT_KEY} from "../Helper";

@Injectable()
export class MainService {

    constructor(private http: Http, private router: Router) {
    }

    getDashboardInfo(courseId: string): Promise<string[][]> {
        let url = Helper.buildUrl('getSummary');
        let postPayload = JSON.stringify({CourseId: courseId});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let months = response.json()['Months'];
                    let data = response.json()['AttendanceCount'];

                    return [months, data];
                }
            },
            reject => {
                console.log(reject);
            });
    }

    getCourseList(userId: string): Promise<Course[]> {
        let url = Helper.buildUrl('getCourses');
        let postPayload = JSON.stringify({UserId: userId});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let courses: Course[] = [];
                    let res = response.json()['CourseList'];
                    res.forEach((result) => {
                        courses.push(new Course(result.CourseId, result.Year, result.Semester));
                    });
                    return courses;
                }
            },
            reject => {
                console.log(reject);
                return [];
            });
    }

    getTimeSlot(courseId: string): Promise<TimeSlot[]> {
        let url = Helper.buildUrl('getDates');
        let postPayload = JSON.stringify({CourseId: courseId});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let timeSlots: TimeSlot[] = [];
                    let res = response.json()['DateList'];
                    res.forEach((result) => {
                        let date = new Date(result).toDateString();
                        timeSlots.push(new TimeSlot(result, result));
                    });

                    return timeSlots;
                }
            },
            reject => {
                console.log(reject);
            });
    }

    getStudentListByDate(date: string): Promise<string[][]> {
        let url = Helper.buildUrl('getStudentList');
        let dateStr = new Date(date);
        let courseId = localStorage.getItem(COURSE_SELECT_KEY);
        let postPayload = JSON.stringify({CourseId: courseId, Date: dateStr.toISOString()});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let stu = response.json()['StudentList'];
                    let timeInMilis = response.json()['TimeList'];
                    let times = [];
                    timeInMilis.forEach(item => {
                        times.push(new Date(item * 1000).toTimeString().substring(0, 5));
                    });

                    return [stu, timeInMilis, times];
                }
            },
            reject => {
                console.log(reject);
            });
    }

    getStudentList(): Promise <Student[] > {
        let url = Helper.buildUrl('getStudentListByCourse');
        let courseId = localStorage.getItem(COURSE_SELECT_KEY);
        let year = localStorage.getItem(COURSE_YEAR_SELECT_KEY);
        let semester = localStorage.getItem(COURSE_SEM_SELECT_KEY);

        let postPayload = JSON.stringify({CourseId: courseId, Year: year, Semester: semester});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        console.log(postPayload);
        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    let students: Student[] = [];
                    let res = response.json()['StudentList'];
                    res.forEach((result) => {
                        students.push(new Student(result, ''));
                    });

                    return students;
                }
            }
        );
    }

    addNewCourse(data): Promise <boolean> {
        let url = Helper.buildUrl('addCourse');
        let postPayload = JSON.stringify(data);
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        console.log(postPayload);
        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {

                    return true;
                }
            });
    }

    addStudentToCourse(students: string): Promise <boolean> {
        let url = Helper.buildUrl('addStudent');

        let courseId = localStorage.getItem(COURSE_SELECT_KEY);
        let year = localStorage.getItem(COURSE_YEAR_SELECT_KEY);
        let semester = localStorage.getItem(COURSE_SEM_SELECT_KEY);

        let postPayload = JSON.stringify({CourseId: courseId, Year: year, Semester: semester, StudentList: [students]});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        console.log(postPayload);
        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    // let res = response.json();

                    return true;
                }
            });
    }

    removeStudent(students: string): Promise <boolean> {
        let url = Helper.buildUrl('removeStudent');

        let courseId = localStorage.getItem(COURSE_SELECT_KEY);
        let year = localStorage.getItem(COURSE_YEAR_SELECT_KEY);
        let semester = localStorage.getItem(COURSE_SEM_SELECT_KEY);

        let postPayload = JSON.stringify({CourseId: courseId, Year: year, Semester: semester, StudentList: [students]});
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        console.log(postPayload);
        return this.http.post(url, postPayload, options).toPromise().then(
            response => {
                if (response.status == 200) {
                    // let res = response.json();

                    return true;
                }

                return false;
            });
    }
}