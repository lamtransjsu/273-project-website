import {Component, OnInit} from "@angular/core";
import {Semester} from "../model/Semester";
import {MainService} from "../main/main.service";
import {USER_ID_KEY} from "../Helper";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'course_registration',
    templateUrl: 'course_registration.component.html'
})
export class CourseRegistrationComponent implements OnInit {

    protected semesters: Semester[] = [new Semester('2016', 'Fall'), new Semester('2017', 'Spring')];
    protected years: string[] = ['2016', ' 2017'];
    protected course: any = {};
    protected days: string[] = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
    ];

    constructor(private mainService: MainService, private router: Router) {
    }

    ngOnInit(): void {
        this.course.Day = [];

    }

    onDateSelected(target): void {

        if (target.checked) {
            this.course.Day.push(target.value);
        } else {
            let i = this.course.Day.indexOf(target.value);
            if (i != -1) {
                this.course.Day.splice(i, 1);
            }
        }
    }

    onSubmited(): void {
        this.course.UserId = localStorage.getItem(USER_ID_KEY);
        this.mainService.addNewCourse(this.course).then((response) => {
            if (response) {
                this.router.navigate(['/course-picker']);
            }
        });
    }
}