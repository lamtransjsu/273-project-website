import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {HttpModule} from "@angular/http";
import {CourseListRowComponent} from "./course_picker/course_list_row/course_list_row";
import {FormsModule} from "@angular/forms";
import {LoginComponent} from "./login/login.component";
import {LoginService} from "./login/login.service";
import {TimeSlotRowComponent} from "./main/time-slot-list/time_slot_row/time-slot-row.component";
import {NgModule} from "@angular/core";
import {AppRoutingModule} from "./app-routing.module";
import {CoursePickerComponent} from "./course_picker/course-picker.component";
import {MainComponent} from "./main/main.component";
import {MainService} from "./main/main.service";
import {TimeSlotListComponent} from "./main/time-slot-list/time-slot-list.component";
import {MainRoutingModule} from "./main/main-routing.module";
import {DashboardComponent} from "./main/dashboard/dashboard.component";
import {ChartsModule} from "ng2-charts";
import {StudentListEditComponent} from "./main/student-list-edit/student-list-edit.component";
import {StatByDateComponent} from "./main/time-slot-list/stat-by-date/stat-by-date.component";
import {CourseRegistrationComponent} from "./course_registration/course_registration.component";
import {NKDatetimeModule} from "ng2-datetime/ng2-datetime";

@NgModule({
    imports: [BrowserModule,
        HttpModule,
        FormsModule,
        ChartsModule,
        AppRoutingModule,
        MainRoutingModule,
        NKDatetimeModule
    ],
    declarations: [
        AppComponent,
        CourseListRowComponent,
        LoginComponent,
        TimeSlotRowComponent,
        CoursePickerComponent,
        MainComponent,
        TimeSlotListComponent,
        DashboardComponent,
        StudentListEditComponent,
        StatByDateComponent,
        CourseRegistrationComponent,
    ],
    providers: [
        LoginService,
        MainService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
