package com.sjsu.cmpe273.attendance.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sjsu.cmpe273.attendance.R;

public class ResetActivity extends AppCompatActivity {

    private static final String TAG = "ResetActivity";

    private EditText mEmailText;
    private Button mResetButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        mEmailText = (EditText) findViewById(R.id.reset_email);
        mResetButton = (Button) findViewById(R.id.btn_reset);

        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email= mEmailText.getText().toString();

                if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mEmailText.setError(getString(R.string.valid_email));
                } else {
                    mEmailText.setError(null);
                    // TODO: send the reset request to the server and show a toast
                    Intent intent = new Intent(ResetActivity.this,
                            LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}