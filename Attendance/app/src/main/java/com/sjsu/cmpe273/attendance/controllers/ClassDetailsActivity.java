package com.sjsu.cmpe273.attendance.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.widget.TextView;

import com.sjsu.cmpe273.attendance.R;

import java.util.ArrayList;

public class ClassDetailsActivity extends AppCompatActivity {

    private TextView mCourseIdText;
    private TextView mStartTimeText;
    private TextView mDaysText;
    private TextView mYearText;
    private TextView mSemesterText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courseinfo);

        // retrieve course detail
        Intent intent = getIntent();
        final String courseName = intent.getStringExtra(CourseActivity.COURSE_ID);
        final String startTime = intent.getStringExtra(CourseActivity.COURSE_START_TIME);
        final String year = intent.getStringExtra(CourseActivity.COURSE_YEAR);
        final String semester = intent.getStringExtra(CourseActivity.COURSE_SEMESTER);
        final ArrayList<String> days = intent.getStringArrayListExtra(CourseActivity.COURSE_DAY);

        StyleSpan b = new StyleSpan(android.graphics.Typeface.BOLD);

        mCourseIdText = (TextView) findViewById(R.id.courseId_text);
        mCourseIdText.setText(courseName);

        mStartTimeText = (TextView) findViewById(R.id.classTime_text);
        String sTime = getString(R.string.class_start_time, startTime);
        SpannableStringBuilder sb1 = new SpannableStringBuilder(sTime);
        sb1.setSpan(b, 0, 11, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mStartTimeText.setText(sb1);

        mDaysText = (TextView) findViewById(R.id.days_text);
        String daysStr = days.toString();
        String cDaysTrimmed = daysStr.substring(1, daysStr.length()-1);
        String cDays = getString(R.string.class_days, cDaysTrimmed);
        SpannableStringBuilder sb2 = new SpannableStringBuilder(cDays);
        sb2.setSpan(b, 0, 7, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mDaysText.setText(sb2);

        mSemesterText = (TextView) findViewById(R.id.semester_text);
        String cSemester = getString(R.string.class_semester, semester);
        SpannableStringBuilder sb3 = new SpannableStringBuilder(cSemester);
        sb3.setSpan(b, 0, 9, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mSemesterText.setText(sb3);

        mYearText = (TextView) findViewById(R.id.year_text);
        String cYear = getString(R.string.class_year, year);
        SpannableStringBuilder sb4 = new SpannableStringBuilder(cYear);
        sb4.setSpan(b, 0, 5, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        mYearText.setText(sb4);
    }
}
