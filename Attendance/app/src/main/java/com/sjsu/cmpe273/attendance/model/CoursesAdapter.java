package com.sjsu.cmpe273.attendance.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sjsu.cmpe273.attendance.R;

import java.util.ArrayList;

public class CoursesAdapter extends ArrayAdapter<CourseInfo> {
    public CoursesAdapter(Context context, ArrayList<CourseInfo> courses) {
        super(context, 0, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CourseInfo course = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_course, parent, false);
        }

        TextView courseIdText = (TextView) convertView.findViewById(R.id.text_course_id);
        TextView classTimeText = (TextView) convertView.findViewById(R.id.text_class_time);

        courseIdText.setText(course.getCourseId());
        classTimeText.setText(course.getClassTime());

        return convertView;
    }
}
