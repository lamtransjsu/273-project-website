package com.sjsu.cmpe273.attendance.controllers;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sjsu.cmpe273.attendance.R;
import com.sjsu.cmpe273.attendance.utils.AppConfig;
import com.sjsu.cmpe273.attendance.utils.DeviceUuidFactory;
import com.sjsu.cmpe273.attendance.utils.SQLiteHandler;
import com.sjsu.cmpe273.attendance.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.sjsu.cmpe273.attendance.controllers.AttendanceCheckerActivity.COLOR_LIGHT_BLUE;
import static com.sjsu.cmpe273.attendance.controllers.LoginActivity.FIRST_NAME;
import static com.sjsu.cmpe273.attendance.controllers.LoginActivity.STUDENT_ID;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";
    private static final String TAG_SIGNUP_REQUEST = "req_signup";

    private static final int MIN_PASSWORD_LENGTH = 4;
    private static final int MAX_PASSWORD_LENGTH = 10;
    private static final int STUDENT_ID_LENGTH = 9;

    private EditText mFirstNameText;
    private EditText mLastNameText;
    private EditText mStudentIdText;
    private EditText mEmailText;
    private EditText mPasswordText;
    private EditText mRetypedPasswordText;
    private Button mSignupButton;
    private TextView mLoginLink;
    private ProgressDialog mProgressDialog;
    private SessionManager mSessionManager;
    private SQLiteHandler mDbHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mFirstNameText = (EditText) findViewById(R.id.student_firstname);
        mLastNameText = (EditText) findViewById(R.id.student_lastname);
        mStudentIdText = (EditText) findViewById(R.id.student_id);
        mEmailText = (EditText) findViewById(R.id.student_email);
        mPasswordText = (EditText) findViewById(R.id.student_password);
        mRetypedPasswordText = (EditText) findViewById(R.id.student_retype_password);
        mSignupButton = (Button) findViewById(R.id.btn_signup);

        mLoginLink = (TextView) findViewById(R.id.link_login);
        String loginString = getString(R.string.already_member_text);
        SpannableString spannedLoginString = new SpannableString(loginString);
        spannedLoginString.setSpan(new ForegroundColorSpan(Color.parseColor(COLOR_LIGHT_BLUE)), 18,
                spannedLoginString.length(), 0);
        mLoginLink.setText(spannedLoginString);

        mDbHandler = new SQLiteHandler(getApplicationContext());
        mSessionManager = new SessionManager(getApplicationContext());

        mProgressDialog = new ProgressDialog(SignupActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        mSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstName = mFirstNameText.getText().toString();
                String lastName = mLastNameText.getText().toString();
                String studentId = mStudentIdText.getText().toString();
                String email = mEmailText.getText().toString();
                String password = mPasswordText.getText().toString();
                String retypedPassword = mRetypedPasswordText.getText().toString();

                if (validate(firstName, lastName, studentId, email, password, retypedPassword)) {
                    // send the signup request to server
                    signUp();
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.signup_failed), Toast.LENGTH_LONG).show();
                }
            }
        });

        mLoginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Login activity
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void signUp() {
        Log.d(TAG, "signUp");
//        {
//            "UserId" : "123456789",
//                "Password" : "1234",
//                "IMEI" : "1234",
//                "FirstName" : "Jack",
//                "LastName" : "Daniels",
//                "EmailId" : "jack@daniels.com"
//        }

        mSignupButton.setEnabled(false);
        mProgressDialog.setMessage(getString(R.string.signing));
        showDialog();

        String tag_string_req = "req_signup";

        final String firstName = mFirstNameText.getText().toString();
        final String lastName = mLastNameText.getText().toString();
        final String studentId = mStudentIdText.getText().toString();
        final String email = mEmailText.getText().toString();
        final String password = mPasswordText.getText().toString();

        DeviceUuidFactory deviceUuid = new DeviceUuidFactory(getApplicationContext());
        final String uniqueDeviceId = deviceUuid.getDeviceUuid().toString();

        // set signUp request parameters
        final Map<String, String> postParam = new HashMap<>();
        postParam.put("UserId", studentId);
        postParam.put("IMEI", uniqueDeviceId);
//        postParam.put("IMEI", "1234"); /*Yassaman: Hack for test*/
        postParam.put("Password", password);
        postParam.put("FirstName", firstName);
        postParam.put("LastName", lastName);
        postParam.put("EmailId", email);

        // send the post request
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_SIGN_UP, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        mSignupButton.setEnabled(true);
                        clearInputFields();

                        if (response == null) {
                            Log.d(TAG, "Response is null");
                            onSigupFailed();
                            return;
                        }

                        Log.d(TAG, "Sin up response: " + response.toString());
                        try {
                            String message = response.getString("Message");
                            if (!message.toLowerCase().equals("success")) {
                                Log.d(TAG, "Message: " + message);
                                onSigupFailed();
                                return;
                            }

                            // add student info to db
                            String fName = postParam.get("FirstName");
                            String lastName = postParam.get("LastName");
                            String sId = postParam.get("UserId");
                            String emailId = postParam.get("EmailId");

                            // make sure there's only one user in DB
                            mDbHandler.deleteUsers();
                            mDbHandler.addStudent(fName
                                    , lastName,
                                    emailId,
                                    sId);

//                            mSessionManager.setLogin(true);

                            // pass name and student id to CourseActivity
                            Intent intent = new Intent(SignupActivity.this,
                                    CourseActivity.class);
                            intent.putExtra(FIRST_NAME, fName);
                            intent.putExtra(STUDENT_ID, sId);
                            startActivity(intent);
                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "Internal error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(), "Internal error", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error response: " + error.getMessage());
                onSigupFailed();
                mPasswordText.setText("");
                mRetypedPasswordText.setText("");
                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG_SIGNUP_REQUEST);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests(TAG_SIGNUP_REQUEST);
    }

    private void showDialog() {
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    private void hideDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private void onSigupFailed() {
        Toast.makeText(getBaseContext(), "Failed to sign up. Try again later.", Toast.LENGTH_LONG).show();
        mSignupButton.setEnabled(true);
    }

    private boolean validate(String firstName, String lastName, String studentId,
                             String email, String password, String retypedPassword) {
        boolean valid = true;

        if (firstName.isEmpty()) {
            mFirstNameText.setError(getString(R.string.valid_firstname));
            valid = false;
        } else {
            mFirstNameText.setError(null);
        }

        if (lastName.isEmpty()) {
            mLastNameText.setError(getString(R.string.valid_lastname));
            valid = false;
        } else {
            mLastNameText.setError(null);
        }

        if (studentId.isEmpty() || studentId.length() != STUDENT_ID_LENGTH) {
            mStudentIdText.setError(getString(R.string.valid_id));
            valid = false;
        } else {
            mStudentIdText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailText.setError(getString(R.string.valid_email));
            valid = false;
        } else {
            mEmailText.setError(null);
        }

        if (password.isEmpty() || password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH) {
            mPasswordText.setError(getString(R.string.valid_password));
            valid = false;
        } else {
            mPasswordText.setError(null);
        }

        if (retypedPassword.isEmpty() || !retypedPassword.equals(password)) {
            mRetypedPasswordText.setError(getResources().getString(R.string.valid_retyped_password));
            valid = false;
        } else {
            mRetypedPasswordText.setError(null);
        }

        return valid;
    }

    private void clearInputFields() {
        mFirstNameText.setText("");
        mLastNameText.setText("");
        mStudentIdText.setText("");
        mEmailText.setText("");
        mPasswordText.setText("");
        mRetypedPasswordText.setText("");
    }
}
