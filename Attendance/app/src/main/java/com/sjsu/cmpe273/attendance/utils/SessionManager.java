package com.sjsu.cmpe273.attendance.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

// Reference: http://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/
public class SessionManager {

    private static String TAG = "SessionManager";

    private SharedPreferences mPref;

    private Editor mEditor;
    private Context mContext;

    private static final String PREF_NAME = "AttendanceStudentLogin";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";

    public SessionManager(Context context) {
        this.mContext = context;
        mPref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mEditor = mPref.edit();
    }

    // if remember me check box is checked call this
    public void setLogin(boolean isLoggedIn) {
        mEditor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);
        mEditor.commit();

        Log.d(TAG, "User login session was modified!");
    }

    public boolean isLoggedIn(){
        // default value is false meaning that keep me logged out
        return mPref.getBoolean(KEY_IS_LOGGED_IN, false);
    }
}
