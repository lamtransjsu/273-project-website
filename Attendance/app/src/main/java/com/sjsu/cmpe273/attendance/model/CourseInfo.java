package com.sjsu.cmpe273.attendance.model;

import java.util.ArrayList;

public class CourseInfo {

    private String courseId;
    private String semester;
    private String year;
    private ArrayList<String> day;
    private String classTime;

    public CourseInfo(String courseId, String semester, String year, ArrayList<String> dayList, String classTime) {
        this.courseId = courseId;
        this.semester = semester;
        this.year = year;
        this.classTime = classTime;

        this.day = new ArrayList<>();
        this.day.addAll(dayList);
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setDay(ArrayList<String> dayList) {
        this.day.addAll(dayList);
    }

    public void setClassTime(String classTime) {
        this.classTime = classTime;
    }

    public String getCourseId() {

        return courseId;
    }

    public String getSemester() {
        return semester;
    }

    public String getYear() {
        return year;
    }

    public ArrayList<String> getDay() {
        return day;
    }

    public String getClassTime() {
        return classTime;
    }
}
