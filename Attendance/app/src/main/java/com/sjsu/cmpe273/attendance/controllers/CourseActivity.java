package com.sjsu.cmpe273.attendance.controllers;

import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sjsu.cmpe273.attendance.R;
import com.sjsu.cmpe273.attendance.model.CourseInfo;
import com.sjsu.cmpe273.attendance.model.CoursesAdapter;
import com.sjsu.cmpe273.attendance.utils.AppConfig;
import com.sjsu.cmpe273.attendance.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.sjsu.cmpe273.attendance.controllers.LoginActivity.FIRST_NAME;
import static com.sjsu.cmpe273.attendance.controllers.LoginActivity.STUDENT_ID;

public class CourseActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String COURSE_ID = "course_id";
    public static final String COURSE_START_TIME = "course_start_time";
    public static final String COURSE_DAY = "course_day";
    public static final String COURSE_YEAR = "course_year";
    public static final String COURSE_SEMESTER = "course_semester";

    private static final String TAG = "CourseActivity";
    private static final String TAG_GET_COURSES_REQUEST = "req_courses";

    private ListView mCourseListView;
    private SessionManager mSessionManager;
    private ArrayAdapter<CourseInfo> mCourseArrayAdapter;
    ArrayList<CourseInfo> mCourseInfoArray;
    private String mStudentId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);

        mSessionManager = new SessionManager(getApplicationContext());

        mCourseListView = (ListView) findViewById(R.id.course_listview);
        mCourseListView.setClickable(true);
        mCourseListView.setOnItemClickListener(this);

        mCourseInfoArray = new ArrayList<>();
        mCourseArrayAdapter = new CoursesAdapter(this, mCourseInfoArray);
        mCourseListView.setAdapter(mCourseArrayAdapter);

        // Empty list, o courses has been scheduled for today
        TextView emptyListText = (TextView)findViewById(R.id.emptyListText);
        mCourseListView.setEmptyView(emptyListText);

        if (!isNetworkAvailable()) {
            emptyListText.setText(R.string.no_network_connection);
        } else {
            emptyListText.setText(R.string.no_courses);
        }

        // get student id from intent
        Intent intent = getIntent();
        String studentName = intent.getStringExtra(FIRST_NAME);
        mStudentId = intent.getStringExtra(STUDENT_ID);

        TextView greetingText = (TextView) findViewById(R.id.greeting_textView);
        String greetingString = getString(R.string.greeting, studentName);
        SpannableString spannedGreetingString = new SpannableString(greetingString);
        spannedGreetingString.setSpan(new ForegroundColorSpan(Color.parseColor(AttendanceCheckerActivity.COLOR_LIGHT_BLUE)), 5,
                spannedGreetingString.length(), 0);
        greetingText.setText(spannedGreetingString);

        // add courses to the adapter, if any
        getCourses(mStudentId);

        // test
//        mCourseInfoArray.add(new CourseInfo("CMPE273", "Spring", "2016", "Wednesday", "17:30"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.app_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout_option:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {

        mSessionManager.setLogin(false);
        Intent intent = new Intent(CourseActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        // start Attendance Activity and pass course name and info
        CourseInfo cInfo = (CourseInfo)adapterView.getItemAtPosition(i);
        Intent intent = new Intent(CourseActivity.this,
                AttendanceCheckerActivity.class);
        intent.putExtra(COURSE_ID, cInfo.getCourseId());
        intent.putExtra(COURSE_START_TIME, cInfo.getClassTime());
        intent.putExtra(COURSE_YEAR, cInfo.getYear());
        intent.putExtra(COURSE_SEMESTER, cInfo.getSemester());
        intent.putExtra(LoginActivity.STUDENT_ID, mStudentId);
        intent.putStringArrayListExtra(COURSE_DAY, cInfo.getDay());

        startActivity(intent);
    }

    private void clearList() {
        mCourseArrayAdapter.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearList();
    }

    private void getCourses(String studentId) {
        Log.d(TAG, "getCourses for " + studentId);

        mCourseArrayAdapter.clear();
        // set getCourses request parameters
        Map<String, String> postParam = new HashMap<>();
        postParam.put("UserId", studentId);

        // send the post request
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_GET_COURSES, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    /* {"Message" : "Success" , "CourseList" : []}
                       {"Course" : "CourseId", "Semester" : "Semester",
                        "Year" : "Year", "Day" : "Day", "ClassTime" : "18:00"} */

                    @Override
                    public void onResponse(JSONObject response) {
                        if (response == null) {
                            Log.d(TAG, "Response is null");
                            onGetCoursesFailed("No response");
                            return;
                        }

                        Log.d(TAG, "getCourses response: " + response.toString());
                        try {
                            String message = response.getString("Message");
                            if (!message.toLowerCase().equals("success")) {
                                Log.d(TAG, "Message: " + message);
                                 onGetCoursesFailed(message);
                                return;
                            }

                            // get courses
                            JSONArray courseList = response.getJSONArray("CourseList");
                            if (courseList != null) {
                                for (int i = 0; i < courseList.length(); i++) {
                                    JSONObject c = courseList.getJSONObject(i);
                                    // get class days
                                    ArrayList<String> days= new ArrayList<>();
                                    JSONArray dayJsonArray = c.getJSONArray("Day");
                                    if (dayJsonArray != null) {
                                        int len = dayJsonArray.length();
                                        for (int j=0; j<len; j++){
                                            days.add(dayJsonArray.get(j).toString());
                                        }
                                    }
                                    // construct the classInfo object
                                    CourseInfo cInfo = new CourseInfo(c.getString("Course"),
                                            c.getString("Semester"),
                                            c.getString("Year"),
                                            days,
                                            c.getString("ClassTime"));
                                    mCourseArrayAdapter.add(cInfo);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "Internal error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(), "Internal error", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error response: " + error.getMessage());
                onGetCoursesFailed(error.getLocalizedMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                int statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };
        // Adding request to request queue
        Log.d(TAG, jsonObjReq.toString());
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG_GET_COURSES_REQUEST);
    }

    private void onGetCoursesFailed(String msg) {
        mCourseArrayAdapter.clear();
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
