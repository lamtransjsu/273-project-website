package com.sjsu.cmpe273.attendance.utils;

public class AppConfig {

    public static String URL_LOGIN = "https://appserver-raspberry.herokuapp.com/mobile/login";
    public static String URL_SIGN_UP = "https://appserver-raspberry.herokuapp.com/mobile/register";
    public static String URL_GET_COURSES = "https://appserver-raspberry.herokuapp.com/mobile/getCourses";

    public static String APPLICATION_JSON = "application/json; charset=utf-8";
}
