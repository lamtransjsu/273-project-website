package com.sjsu.cmpe273.attendance.controllers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sjsu.cmpe273.attendance.R;
import com.sjsu.cmpe273.attendance.utils.AppConfig;
import com.sjsu.cmpe273.attendance.utils.DeviceUuidFactory;
import com.sjsu.cmpe273.attendance.utils.SQLiteHandler;
import com.sjsu.cmpe273.attendance.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.sjsu.cmpe273.attendance.controllers.AttendanceCheckerActivity.COLOR_LIGHT_BLUE;

// Reference: http://sourcey.com/beautiful-android-login-and-signup-screens-with-material-design/
public class LoginActivity extends AppCompatActivity {

    public static final String FIRST_NAME = "first_name";
    public static final String STUDENT_ID = "student_id";

    private static final String TAG = "LoginActivity";
    private static final String TAG_LOGIN_REQUEST = "req_login";

    private static final int MIN_PASSWORD_LENGTH = 4;
    private static final int MAX_PASSWORD_LENGTH = 10;
    private static final int STUDENT_ID_LENGTH = 9;

    private EditText mStudentIdText;
    private EditText mPasswordText;
    private CheckBox mRememberMeCheck;
    private Button mLoginButton;
    private TextView mSignUpLink;
    private TextView mResetLink;
    private SQLiteHandler mDbHandler;
    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mStudentIdText = (EditText) findViewById(R.id.input_id);
        mPasswordText = (EditText) findViewById(R.id.input_password);
        mRememberMeCheck = (CheckBox) findViewById(R.id.input_remember);
        mLoginButton = (Button) findViewById(R.id.btn_login);

        mResetLink = (TextView) findViewById(R.id.link_reset);
        mSignUpLink = (TextView) findViewById(R.id.link_signup);
        String createAccountString = getString(R.string.create_account);
        SpannableString spannedAccountString = new SpannableString(createAccountString);
        spannedAccountString.setSpan(new ForegroundColorSpan(Color.parseColor(COLOR_LIGHT_BLUE)), 23,
                spannedAccountString.length(), 0);
        mSignUpLink.setText(spannedAccountString);

        mDbHandler = new SQLiteHandler(getApplicationContext());
        mSessionManager = new SessionManager(getApplicationContext());

        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AppController.getInstance().cancelPendingRequests(TAG_LOGIN_REQUEST);
                dialog.dismiss();
                mLoginButton.setEnabled(true);
            }
        });

        // check login status, if logged in, show courses list
        if (mSessionManager.isLoggedIn()) {

            HashMap<String, String> detailsMap = mDbHandler.getUserDetails();
            String sId = detailsMap.get(SQLiteHandler.KEY_STUDENT_ID);
            String fName = detailsMap.get(SQLiteHandler.KEY_FIRST_NAME);

            Intent intent = new Intent(LoginActivity.this, CourseActivity.class);
            intent.putExtra(FIRST_NAME, fName);
            intent.putExtra(STUDENT_ID, sId);

            startActivity(intent);
            finish();
        }

        mRememberMeCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    mSessionManager.setLogin(true);
                }
            }
        });
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    onLoginFailed();
                } else {
                     login();
                    // for test
//                    Intent intent = new Intent(LoginActivity.this,
//                            CourseActivity.class);
//                    intent.putExtra(FIRST_NAME, "Jack");
//                    intent.putExtra(STUDENT_ID, "123456789");
//                    startActivity(intent);
                }
            }
        });

        mSignUpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
            }
        });

        mResetLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Reset activity
                Intent intent = new Intent(getApplicationContext(), ResetActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login() {
        Log.d(TAG, "Login");

        // start the progress bar
        mLoginButton.setEnabled(false);
        mProgressDialog.setMessage(getString(R.string.authenticating));
        showDialog();

        // get user input and device unique id
        final String studentId = mStudentIdText.getText().toString();
        final String password = mPasswordText.getText().toString();

        DeviceUuidFactory deviceUuid = new DeviceUuidFactory(getApplicationContext());
        final String uniqueDeviceId = deviceUuid.getDeviceUuid().toString();

        // set login request parameters
        Map<String, String> postParam = new HashMap<>();
        postParam.put("UserId", studentId);
        postParam.put("Password", password);
        postParam.put("IMEI", uniqueDeviceId);
//         postParam.put("IMEI", "1234"); /*Yassaman: Hack for test*/

        // send the post request
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new JSONObject(postParam),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        /* {
                                "Message": "Success",
                                "UserId": "123456789",
                                "Password": "1234",
                                "FirstName": "Jack",
                                "LastName": "Daniels",
                                "EmailId": "jack@daniels.com",
                                "IMEI": "1234"
                        } */
                        hideDialog();
                        mLoginButton.setEnabled(true);
                        clearInputFields();

                        if (response == null) {
                            Log.d(TAG, "Response is null");
                            onLoginFailed();
                            return;
                        }

                        Log.d(TAG, "Login response: " + response.toString());
                        try {
                            String message = response.getString("Message");
                            if (!message.toLowerCase().equals("success")) {
                                Log.d(TAG, "Message: " + message);
                                onLoginFailed();
                                return;
                            }

                            // add student info to db
                            String fName = response.getString("FirstName");
                            String sId = response.getString("UserId");
                            // make sure there's only one user in DB
                            mDbHandler.deleteUsers();
                            mDbHandler.addStudent(fName
                                    , response.getString("LastName"),
                                    response.getString("EmailId"),
                                    sId);

                            // pass name and student id to CourseActivity
                            Intent intent = new Intent(LoginActivity.this,
                                    CourseActivity.class);
                            intent.putExtra(FIRST_NAME, fName);
                            intent.putExtra(STUDENT_ID, sId);
                            startActivity(intent);

                            // check if user should stay logged in
                            if (mRememberMeCheck.isChecked()) {
                                mSessionManager.setLogin(true);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "Internal error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(), "Internal error", Toast.LENGTH_LONG).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error response: " + error.getMessage());
                onLoginFailed();
                mPasswordText.setText("");
                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    int statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }
        };
        // Adding request to request queue
        Log.d(TAG, jsonObjReq.toString());
        AppController.getInstance().addToRequestQueue(jsonObjReq, TAG_LOGIN_REQUEST);
    }

    private void showDialog() {
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    private void hideDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        // send application to background
        moveTaskToBack(true);
    }

    private void onLoginFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.login_failed), Toast.LENGTH_LONG).show();
        mLoginButton.setEnabled(true);
    }

    private boolean validate() {
        boolean valid = true;

        String studentId = mStudentIdText.getText().toString();
        String password = mPasswordText.getText().toString();

        if (studentId.isEmpty() || studentId.length() != STUDENT_ID_LENGTH) {
            mStudentIdText.setError(getString(R.string.valid_id));
            valid = false;
        } else {
            mStudentIdText.setError(null);
        }

        if (password.isEmpty() || password.length() < MIN_PASSWORD_LENGTH || password.length() > MAX_PASSWORD_LENGTH) {
            mPasswordText.setError(getString(R.string.valid_password));
            valid = false;
        } else {
            mPasswordText.setError(null);
        }

        return valid;
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                onLoginFailed();
                hideDialog();
            }
        };
    }

    private void clearInputFields() {
        mStudentIdText.setText("");
        mPasswordText.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppController.getInstance().cancelPendingRequests(TAG_LOGIN_REQUEST);
    }
}
