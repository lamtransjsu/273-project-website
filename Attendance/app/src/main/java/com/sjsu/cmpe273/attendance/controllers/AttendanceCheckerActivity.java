package com.sjsu.cmpe273.attendance.controllers;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sjsu.cmpe273.attendance.R;
import com.sjsu.cmpe273.attendance.utils.DeviceUuidFactory;
import com.sjsu.cmpe273.attendance.utils.TimeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import static android.widget.Toast.makeText;
import static com.sjsu.cmpe273.attendance.controllers.CourseActivity.COURSE_DAY;
import static com.sjsu.cmpe273.attendance.controllers.CourseActivity.COURSE_ID;
import static com.sjsu.cmpe273.attendance.controllers.CourseActivity.COURSE_SEMESTER;
import static com.sjsu.cmpe273.attendance.controllers.CourseActivity.COURSE_START_TIME;
import static com.sjsu.cmpe273.attendance.controllers.CourseActivity.COURSE_YEAR;
import static com.sjsu.cmpe273.attendance.controllers.LoginActivity.STUDENT_ID;

public class AttendanceCheckerActivity extends AppCompatActivity {

    public static final int MINUTES_BEFORE = 60;
    public static final int MINUTES_AFTER = 60;
    private static final String RP_NAME = "raspberrypi";
    private static final String RP_MAC_ADDRESS = "B8:27:EB:DA:F9:5B";
    private static final String TAG = "AttendanceActivity";
    public static final String COLOR_LIGHT_BLUE = "#1394C6";

    private Button mCheckinButton;
    private TextView mModuleText;
    private TextView mCheckinCourseText;
    private ProgressDialog mProgressDialog;

    // exclamation mark with ascii code 33 is used as the end of message tag
    final byte DELIMETER = 33;
    int READ_BUFFER_POSITION = 0;

    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice = null;

    private ImageView mNotificationIcon;

    private String mCourseName;
    private String mStudentId;
    private String mStartTime;
    private String mYear;
    private String mSemester;
    private ArrayList<String> mDays;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString(COURSE_ID, mCourseName);
        savedInstanceState.putString(COURSE_START_TIME, mStartTime);
        savedInstanceState.putString(COURSE_YEAR, mYear);
        savedInstanceState.putString(COURSE_SEMESTER, mSemester);
        savedInstanceState.putString(LoginActivity.STUDENT_ID, mStudentId);
        savedInstanceState.putStringArrayList(COURSE_DAY, mDays);

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        final Handler handler = new Handler();

        // set the module string
        String today = TimeUtils.getCurrentTime24();
        String moduleString = getString(R.string.module_number, today);
        SpannableString spannedModuleString = new SpannableString(moduleString);
        spannedModuleString.setSpan(new ForegroundColorSpan(Color.parseColor(COLOR_LIGHT_BLUE)), 7,
                moduleString.length(), 0);
        mModuleText = (TextView) findViewById(R.id.module_text);
        mModuleText.setText(spannedModuleString);

        mProgressDialog = new ProgressDialog(AttendanceCheckerActivity.this, R.style.PDialogTheme);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            mCourseName = savedInstanceState.getString(COURSE_ID);
            mStudentId = savedInstanceState.getString(STUDENT_ID);
            mStartTime = savedInstanceState.getString(COURSE_START_TIME);
            mYear = savedInstanceState.getString(COURSE_YEAR);
            mSemester = savedInstanceState.getString(COURSE_SEMESTER);
            mDays = savedInstanceState.getStringArrayList(COURSE_DAY);
        } else {
            // retrieve course detail
            Intent intent = getIntent();
            mCourseName = intent.getStringExtra(COURSE_ID);
            mStudentId = intent.getStringExtra(STUDENT_ID);
            mStartTime = intent.getStringExtra(COURSE_START_TIME);
            mYear = intent.getStringExtra(COURSE_YEAR);
            mSemester = intent.getStringExtra(COURSE_SEMESTER);
            mDays = intent.getStringArrayListExtra(COURSE_DAY);
        }

        // check if it's too early or late to check into the class

        boolean canCheckin = false;
        long timeDiff = TimeUtils.getTimeDifferenceMinutes(mStartTime);
        if (timeDiff> -1*MINUTES_AFTER && timeDiff< MINUTES_BEFORE) {
            canCheckin = true;
        } else {
            canCheckin = false;
        }
//        canCheckin= true; /*Yassaman: Hack for test*/
        mCheckinButton = (Button) findViewById(R.id.btn_checkin);
        mCheckinButton.setEnabled(canCheckin);

        mCheckinCourseText = (TextView) findViewById(R.id.attendance_text);
        mNotificationIcon = (ImageView) findViewById(R.id.notification_icon);
        if (canCheckin) {
            // notification icon
//            mNotificationIcon.setVisibility(View.INVISIBLE);
            mNotificationIcon.setImageResource(R.drawable.ic_checkmark);
            // set the info text
            String attendanceString = getString(R.string.mark_attended, mCourseName);
            SpannableString spannedAttendingString = new SpannableString(attendanceString);
            spannedAttendingString.setSpan(new ForegroundColorSpan(Color.parseColor(COLOR_LIGHT_BLUE)), 35,
                    attendanceString.length() - 1, 0);
            mCheckinCourseText.setText(spannedAttendingString);
        } else {
            // check mark icon
//            mNotificationIcon.setVisibility(View.VISIBLE);
            mNotificationIcon.setImageResource(R.drawable.ic_notification);
            // set the info text
            String attendanceString = getString(R.string.too_soon_or_late, mCourseName);
            SpannableString spannedAttendingString = new SpannableString(attendanceString);
            spannedAttendingString.setSpan(new ForegroundColorSpan(Color.parseColor(COLOR_LIGHT_BLUE)), 48,
                    attendanceString.length(), 0);
            mCheckinCourseText.setText(spannedAttendingString);
        }

        mCheckinCourseText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AttendanceCheckerActivity.this,
                        ClassDetailsActivity.class);
                intent.putExtra(COURSE_ID, mCourseName);
                intent.putExtra(COURSE_START_TIME, mStartTime);
                intent.putStringArrayListExtra(COURSE_DAY, mDays);
                intent.putExtra(COURSE_SEMESTER, mSemester);
                intent.putExtra(COURSE_YEAR, mYear);
                startActivity(intent);
            }
        });

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        final class workerThread implements Runnable {

            private String btMsg;

            public workerThread(String msg) {
                btMsg = msg;
            }

            public void run() {
                sendBtMsg(btMsg);
                while (!Thread.currentThread().isInterrupted()) {
                    int bytesAvailable;
                    boolean workDone = false;

                    try {
                        final InputStream mmInputStream;
                        // TODO: check for null pointer
                        if (mmSocket == null) {
                            Log.d(TAG, "mmSocket is null");
                            return;
                        }
                        mmInputStream = mmSocket.getInputStream();

                        if (mmInputStream == null) {
                            Log.i(TAG, "Input Stream is null");
                            return;
                        }

                        bytesAvailable = mmInputStream.available();
                        if (bytesAvailable > 0) {

                            byte[] packetBytes = new byte[bytesAvailable];
                            Log.i(TAG, "bytes available");
                            byte[] readBuffer = new byte[1024];
                            mmInputStream.read(packetBytes);

                            for (int i = 0; i < bytesAvailable; i++) {
                                byte b = packetBytes[i];
                                if (b == DELIMETER) {
                                    byte[] encodedBytes = new byte[READ_BUFFER_POSITION];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    READ_BUFFER_POSITION = 0;
                                    Log.d(TAG, "****** " + data);
                                    //data should have server response now
                                    handler.post(new Runnable() {
                                        public void run() {
                                            // process data, data is received here
                                            // server sends me informative messages
                                            if (data == null || !data.equalsIgnoreCase("success")) {
                                                // Attendance request didn't go through
//                                                mNotificationIcon.setImageResource(R.drawable.ic_notification);
//                                                mNotificationIcon.setVisibility(View.VISIBLE);
                                                hideDialog(data);
                                            } else {
                                                // student was successfully marked as attended
                                                runOnUiThread(new Runnable() {
                                                    public void run() {
                                                        hideDialog("Checked in!");
//                                                        mNotificationIcon.setVisibility(View.VISIBLE);
//                                                        mNotificationIcon.setImageResource(R.drawable.ic_checkmark);
                                                    }
                                                });
                                            }
                                        }
                                    });

                                    workDone = true;
                                    break;
                                } else {
                                    readBuffer[READ_BUFFER_POSITION++] = b;
                                }
                            }

                            if (workDone == true) {
                                mmSocket.close();
                                break;
                            }
                        }
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        hideDialog("An I/O error happened!");
                    }
                }
            }
        }
        ;

        mCheckinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start a worker thread send the data to RP server through bluetooth
//                {"UserId":"99999", "IMEI":"xxxx", "CourseId":"CMPE273-01" }
                JSONObject jsonObj = new JSONObject();
                DeviceUuidFactory deviceUuid = new DeviceUuidFactory(getApplicationContext());
                final String uniqueDeviceId = deviceUuid.getDeviceUuid().toString();

                try {
                    jsonObj.put("CourseId", mCourseName);
                    jsonObj.put("IMEI", uniqueDeviceId);
//                    jsonObj.put("IMEI", "1234"); /*Yassaman: Hack for test*/
                    jsonObj.put("UserId", mStudentId);
                    jsonObj.put("Year", mYear);
                    jsonObj.put("Semester", mSemester);
                    (new Thread(new workerThread(jsonObj.toString()))).start();
                } catch (JSONException e) {
                    makeText(getApplicationContext(), String.format("Internal error. Could not check in {}.", mStudentId),
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (device.getAddress().equals(RP_MAC_ADDRESS)) {
                    mmDevice = device;
                    break;
                }
            }
        }
    }

    public void sendBtMsg(String msg2send) {

        showDialog();
        UUID uuid = UUID.fromString("94f39d29-7d6d-437d-973b-fba39e49d4ee"); //Standard SerialPortService ID
        try {

            if (mmDevice == null) {
                Log.d(TAG, "Couldn't find the device.");
                hideDialog("It seems like devices are not paired :-(");
                return;
            }

            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);

            if (mmSocket == null) {
                Log.d(TAG, "Could not get an Rfcomm socket connection");
                return;
            }

            if (!mmSocket.isConnected()) {
                mmSocket.connect();
            }

            OutputStream mmOutputStream = mmSocket.getOutputStream();
            mmOutputStream.write(msg2send.getBytes());

        }
        catch (IOException e) {
            Log.d(TAG, e.getLocalizedMessage());
            hideDialog("Could not establish a connection");
            Thread.currentThread().interrupt();

        }
    }

    private void showDialog() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (!mProgressDialog.isShowing())
                    mProgressDialog.show();
            }
        });
    }

    private void hideDialog(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                Toast toast = Toast.makeText(AttendanceCheckerActivity.this, msg, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Thread.currentThread() != null) {
            Thread.currentThread().interrupt();
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (Thread.currentThread() != null) {
            Thread.currentThread().interrupt();
        }
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}