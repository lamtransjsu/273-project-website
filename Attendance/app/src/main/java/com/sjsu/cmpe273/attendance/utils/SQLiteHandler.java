package com.sjsu.cmpe273.attendance.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

// reference http://stackoverflow.com/questions/2421189/version-of-sqlite-used-in-android
public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = "SQLiteHandler";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "attendance_api";

    private static final String TABLE_STUDENT = "student";

    public static final String KEY_ID = "id";
    public static final String KEY_FIRST_NAME = "firstname";
    public static final String KEY_LAST_NAME = "lasttname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_STUDENT_ID = "studentid";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_STUDENT + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_FIRST_NAME + " TEXT,"
                + KEY_LAST_NAME + " TEXT,"
                + KEY_EMAIL + " TEXT UNIQUE,"
                + KEY_STUDENT_ID + " TEXT"
                + ")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STUDENT);
        onCreate(db);
    }

    public void addStudent(String firstName, String lastName, String email, String studentId) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, firstName);
        values.put(KEY_LAST_NAME, lastName);
        values.put(KEY_EMAIL, email);
        values.put(KEY_STUDENT_ID, studentId);

        long id = db.insertWithOnConflict(TABLE_STUDENT, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        db.close();
        Log.d(TAG, "New user inserted into sqlite. id: " + id);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> student = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            student.put(KEY_FIRST_NAME, cursor.getString(1));
            student.put(KEY_LAST_NAME, cursor.getString(2));
            student.put(KEY_EMAIL, cursor.getString(3));
            student.put(KEY_STUDENT_ID, cursor.getString(4));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + student.toString());

        return student;
    }

    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_STUDENT, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }
}
