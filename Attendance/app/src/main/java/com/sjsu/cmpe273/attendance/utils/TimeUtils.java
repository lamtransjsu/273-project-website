package com.sjsu.cmpe273.attendance.utils;

import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {

    public static int getYear() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        return year;
    }

    public static String getCurrentTime24() {
        DateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    public static long getTimeDifferenceMinutes(String startTime) {

        LocalTime classTime = LocalTime.parse( startTime ) ;
        LocalTime currentTime = LocalTime.now();

        long minutes = (Minutes.minutesBetween(currentTime, classTime)).getMinutes();

        return minutes;
    }
}
